import { Component } from "react";
import ProductList from "../components/ProductList";
import styles from "./Home.module.scss";
import Header from "../components/Header";

export class Home extends Component {
  constructor(props) {
    super(props);

    this.state = {
      productList: [],
      productsInFavorites: 0,
      productsInCart: 0,
    };
  }

  componentDidMount() {
    fetch("./products.json")
      .then((response) => response.json())
      .then((data) => {
        this.setState({ productList: data });
      })
      .catch((error) => alert(error.message));

    const productsInFavorites = localStorage.getItem("productFavorites")
      ? JSON.parse(localStorage.getItem("productFavorites")).length
      : 0;
    const productsInCart = localStorage.getItem("productInCart")
      ? JSON.parse(localStorage.getItem("productInCart")).length
      : 0;

    this.setState((prevState) => ({
      ...prevState,
      productsInFavorites,
      productsInCart,
    }));
  }

  setFavoritesCount = () => {
    const count = JSON.parse(localStorage.getItem("productFavorites")).length;
    this.setState((prevState) => ({
      ...prevState,
      productsInFavorites: count,
    }));
  };
  setCartCount = () => {
    const count = JSON.parse(localStorage.getItem("productInCart")).length;
    this.setState((prevState) => ({ ...prevState, productsInCart: count }));
  };

  render() {
    return (
      <>
        <Header
          productsInFavorites={this.state.productsInFavorites}
          productsInCart={this.state.productsInCart}
        />
        <main className={styles.main}>
          <section className="container">
            <h2 className={styles.title}>Choose your iPhone!)</h2>
            <ProductList
              productList={this.state.productList}
              setCartCount={this.setCartCount}
              setFavoritesCount={this.setFavoritesCount}
            />
          </section>
        </main>
      </>
    );
  }
}
