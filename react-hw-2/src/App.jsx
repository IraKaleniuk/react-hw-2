import "./App.css";
import { Home } from "./pages";
import styles from "./App.scss";

function App() {
  return (
    <div className={styles.bodyWrap}>
      <Home />
    </div>
  );
}

export default App;
