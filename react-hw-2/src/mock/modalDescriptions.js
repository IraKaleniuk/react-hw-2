export const addToCartModalDesc = {
  header: "Add to cart",
  text: "Do you want to add a product to the cart?",
  closeButton: true,
  actions: [
    { backgroundColor: "#1E5128", text: "Cancel", key: "0" },
    { backgroundColor: "#1E5128", text: "Ok", key: "1" },
  ],
};
export const deleteFromCartModalDesc = {
  header: "Delete from cart",
  text: "Do you want to delete a product from the cart?",
  closeButton: true,
  actions: [
    { backgroundColor: "#1E5128", text: "Cancel", key: "0" },
    { backgroundColor: "#1E5128", text: "Ok", key: "1" },
  ],
};
