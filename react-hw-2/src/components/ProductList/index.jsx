import { Component } from "react";
import PropTypes from "prop-types";
import styles from "./ProductList.module.scss";
import ProductCard from "../ProductCard";
export default class ProductList extends Component {
  constructor(props) {
    super(props);
    this.state = { productList: props.productList, favorites: [], cart: [] };
  }

  componentDidMount() {
    const favorites = localStorage.getItem("productFavorites")
      ? JSON.parse(localStorage.getItem("productFavorites"))
      : [];
    const cart = localStorage.getItem("productInCart")
      ? JSON.parse(localStorage.getItem("productInCart"))
      : [];
    this.setState((prevState) => ({ ...prevState, favorites, cart }));
  }

  componentDidUpdate(prevProps) {
    if (prevProps.productList !== this.props.productList) {
      this.setState({ productList: this.props.productList });
    }
  }

  addToFavorites = (article) => {
    const favorites = this.state.favorites;
    if (!favorites.includes(article)) {
      const newFavorites = [...favorites, article];

      this.setState((prevState) => ({
        ...prevState,
        favorites: newFavorites,
      }));
      localStorage.setItem("productFavorites", JSON.stringify(newFavorites));
    }
  };

  deleteFromFavorites = (article) => {
    const favorites = this.state.favorites;
    const newFavorites = favorites.filter((product) => product !== article);

    this.setState((prevState) => ({
      ...prevState,
      favorites: newFavorites,
    }));
    localStorage.setItem("productFavorites", JSON.stringify(newFavorites));
  };
  addToCart = (article) => {
    const cart = this.state.cart;
    if (!cart.includes(article)) {
      const newCart = [...cart, article];

      this.setState((prevState) => ({
        ...prevState,
        cart: newCart,
      }));
      localStorage.setItem("productInCart", JSON.stringify(newCart));
    }
  };

  deleteFromCart = (article) => {
    const cart = this.state.cart;
    const newCart = cart.filter((product) => product !== article);

    this.setState((prevState) => ({
      ...prevState,
      cart: newCart,
    }));
    localStorage.setItem("productInCart", JSON.stringify(newCart));
  };

  render() {
    return (
      <ul className={styles.productList}>
        {this.state.productList.map((product) => (
          <ProductCard
            key={product.article}
            {...product}
            isFavorite={this.state.favorites.includes(product.article)}
            inCart={this.state.cart.includes(product.article)}
            addToFavorites={this.addToFavorites}
            deleteFromFavorites={this.deleteFromFavorites}
            setFavoritesCount={this.props.setFavoritesCount}
            addToCart={this.addToCart}
            deleteFromCart={this.deleteFromCart}
            setCartCount={this.props.setCartCount}
          />
        ))}
      </ul>
    );
  }
}

ProductList.propTypes = {
  productList: PropTypes.arrayOf(PropTypes.object),
  setCartCount: PropTypes.func,
  setFavoritesCount: PropTypes.func,
};
