import { Component } from "react";
import PropTypes from "prop-types";
import styles from "./ProductCard.module.scss";
import Button from "../Button";
import Modal from "../Modal";
import {
  addToCartModalDesc,
  deleteFromCartModalDesc,
} from "../../mock/modalDescriptions";

export default class ProductCard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      cardData: {
        name: "",
        price: 0,
        imgURL: "",
        article: 0,
        color: "",
        isFavorite: false,
        inCart: false,
      },
      modalIsOpen: false,
    };
  }

  componentDidMount() {
    this.setState((prevState) => ({
      ...prevState,
      cardData: {
        name: this.props.name,
        price: this.props.price,
        imgURL: this.props.imgURL,
        article: this.props.article,
        color: this.props.color,
        isFavorite: this.props.isFavorite,
        inCart: this.props.inCart,
      },
    }));
  }

  setIsFavorite = (article) => {
    this.state.cardData.isFavorite
      ? this.props.deleteFromFavorites(article)
      : this.props.addToFavorites(article);

    this.setState((prevState) => ({
      ...prevState,
      cardData: {
        ...prevState.cardData,
        isFavorite: !prevState.cardData.isFavorite,
      },
    }));
    this.props.setFavoritesCount();
  };
  setInCart = () => {
    this.state.cardData.inCart
      ? this.props.deleteFromCart(this.state.cardData.article)
      : this.props.addToCart(this.state.cardData.article);

    this.setState((prevState) => ({
      ...prevState,
      cardData: {
        ...prevState.cardData,
        inCart: !prevState.cardData.inCart,
      },
    }));
    this.props.setCartCount();
    this.showModal();
  };

  showModal = () => {
    this.setState((prevState) => ({
      ...prevState,
      modalIsOpen: !prevState.modalIsOpen,
    }));
  };

  createModal = (modalData, actionHandlers = []) => {
    const {
      header: modalHeader,
      text: modalText,
      actions: modalActions,
      closeButton: modalCloseBtn,
    } = modalData;
    const actions = modalActions.map((action) => (
      <Button
        backgroundColor={action.backgroundColor}
        text={action.text}
        key={action.key}
        onClick={() => actionHandlers[action.key]}
      />
    ));
    return (
      <Modal
        header={modalHeader}
        text={modalText}
        actions={actions}
        closeButton={modalCloseBtn}
        closeButtonHandler={() => this.showModal}
      />
    );
  };

  render() {
    return (
      <>
        <div className={styles.cardItem}>
          <img
            className={styles.img}
            src={this.state.cardData.imgURL}
            alt="photo"
          />
          <div className={styles.header}>
            <h2 className={styles.title}>{this.state.cardData.name}</h2>
            <svg
              className={styles.favoriteIcon}
              onClick={() => this.setIsFavorite(this.state.cardData.article)}
              width="117"
              height="112"
              viewBox="0 0 117 112"
              fill="none"
              xmlns="http://www.w3.org/2000/svg"
            >
              {this.state.cardData.isFavorite ? (
                <path
                  d="M58.5 1.61804L71.8321 42.65L71.9443 42.9955H72.3076H115.451L80.5473 68.3546L80.2534 68.5681L80.3657 68.9136L93.6977 109.946L58.7939 84.5864L58.5 84.3729L58.2061 84.5864L23.3023 109.946L36.6343 68.9136L36.7466 68.5681L36.4527 68.3546L1.54886 42.9955H44.6924H45.0557L45.1679 42.65L58.5 1.61804Z"
                  fill="#0A84FF"
                  stroke="#0A84FF"
                />
              ) : (
                <path
                  d="M58.5 3.23607L71.3566 42.8045L71.5811 43.4955H72.3076H113.912L80.2534 67.9501L79.6656 68.3771L79.8901 69.0681L92.7467 108.637L59.0878 84.1819L58.5 83.7548L57.9122 84.1819L24.2533 108.637L37.1099 69.0681L37.3344 68.3771L36.7466 67.9501L3.08771 43.4955H44.6924H45.4189L45.6434 42.8045L58.5 3.23607Z"
                  stroke="#0A84FF"
                  strokeWidth="2"
                />
              )}
            </svg>
          </div>
          <div className={styles.description}>
            <div className={styles.color}>
              <span className={styles.label}>Color:</span>
              <span className={styles.value}>{this.state.cardData.color}</span>
            </div>
            <div className={styles.article}>
              <span className={styles.label}>Article:</span>
              <span className={styles.value}>
                {this.state.cardData.article}
              </span>
            </div>
          </div>
          <div className={styles.priceWrap}>
            <p className={styles.price}>{this.state.cardData.price}$</p>
            {this.state.cardData.inCart ? (
              <Button
                backgroundColor="#0A84FF"
                text="Added to cart"
                onClick={() => this.showModal}
              />
            ) : (
              <Button
                backgroundColor="#0A84FF"
                text="Add to cart"
                onClick={() => this.showModal}
              />
            )}
          </div>
        </div>

        {this.state.modalIsOpen &&
          (this.state.cardData.inCart
            ? this.createModal(deleteFromCartModalDesc, [
                this.showModal,
                this.setInCart,
              ])
            : this.createModal(addToCartModalDesc, [
                this.showModal,
                this.setInCart,
              ]))}
      </>
    );
  }
}

ProductCard.propTypes = {
  name: PropTypes.string,
  price: PropTypes.number,
  imgURL: PropTypes.string,
  article: PropTypes.number,
  color: PropTypes.string,
  isFavorite: PropTypes.bool,
  inCart: PropTypes.bool,
  addToFavorites: PropTypes.func,
  deleteFromFavorites: PropTypes.func,
  setFavoritesCount: PropTypes.func,
  addToCart: PropTypes.func,
  deleteFromCart: PropTypes.func,
  setCartCount: PropTypes.func,
};

ProductCard.defaultProps = {
  isFavorite: false,
  inCart: false,
};
