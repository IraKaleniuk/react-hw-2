import { Component } from "react";
import styles from "./Button.module.scss";
import PropTypes from "prop-types";

export default class Button extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <>
        <button
          className={`${styles.button} ${
            this.props.text.toLowerCase() === "add to cart"
              ? styles.addToCart
              : this.props.text.toLowerCase() === "added to cart"
              ? styles.deleteFromCart
              : ""
          }`}
          style={{ background: this.props.backgroundColor }}
          onClick={this.props.onClick()}
        >
          {this.props.text}
        </button>
      </>
    );
  }
}

Button.propTypes = {
  backgroundColor: PropTypes.string,
  text: PropTypes.string,
  onClick: PropTypes.func,
};

Button.defaultProps = {
  backgroundColor: "",
  text: "",
  onClick: function () {
    console.log("Btn clicked!");
  },
};
